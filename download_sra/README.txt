The script launcher_download_sra_v003.slurm interacts over a list of SRA accession ids and it executes the module download_sra_v003.sh to retrieve the corresponding reads from the NCBI platform.

# sbatch launcher_download_sra_v3.slurm \
# complete/path/to/the/project/directory \
#               accession_list.txt
#
# head -1 accession_list.txt
# SRR1550982

An alternative to this script is the tool sra-dump from the SRA-tool kit.
