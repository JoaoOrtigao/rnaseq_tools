#!/usr/bin/bash

#
# download_sra.sh
# Joao Ortigao 18-05-2019 last edition: 05/04/2022
# Download SRA files by run accession using wget
# sh download_sra.sh PROJECTDIR SRR1550981
#

PROJECTDIR=$1 
RUN=$2
SRADIR=${PROJECTDIR}data/download/sra
echo "SRADIR: ${PROJECTDIR}data/download/sra"
LOGDIR=${PROJECTDIR}data/download/log
echo "LOGDIR: ${PROJECTDIR}data/download/log"

# create output directories
if [ ! -d $SRADIR ]; then mkdir -p $SRADIR; fi
if [ ! -d $LOGDIR ]; then mkdir -p $LOGDIR ; fi

COD1=$(echo $RUN | cut -c 1-3) # 3 first characters
COD2=$(echo $RUN | cut -c 1-6) # 6 first characters

FTP_SRA='ftp://ftp-trace.ncbi.nih.gov/sra/sra-instant/reads/ByRun/sra'
echo "# teste: $FTP_SRA/$COD1/$COD2/$RUN/${RUN}.sra"

### load module for SRA Toolkit
module load tools/sratoolkit
which test-sra
which vdb-validate

# if SRA file do not exist, do the download using URL.
if [ ! -f ${SRADIR}/${RUN}.sra ]
then

	# first URL to try the download
	URL="$FTP_SRA/$COD1/$COD2/$RUN/${RUN}.sra"

	# check URL 1 and 2
	if [[ ! $(curl -s --head $URL | head -n 1) ]]; then
		echo "# URL1 not found: $URL"

		# second URL to try the downlod
		URL=$(test-sra $RUN | awk '/Remote FaspHttp/{print $3}' | sort -u)
		if [[ ! $(curl -s --head $URL | head -n 1) ]]; then
			echo "URL2 not found: $URL"
			echo -e "\n###STATUS###\n${RUN}\tFAILURE\tURL NOT FOUND\n#########\n"
			exit 1
		fi

	fi

	# download
	echo; date
	COMMAND="wget -c -o ${LOGDIR}/${RUN}.WGET.log -O ${SRADIR}/${RUN}.sra $URL"
	echo "# executing: ${COMMAND}"
        time $COMMAND

else
	# if SRA file exists, exit here 
        echo -e "\n# File ${SRADIR}/${RUN}.sra exits. Remove file before download again."
	exit 1

fi

# test downloaded file using SRA tools
COMMAND="vdb-validate -q ${SRADIR}/${RUN}.sra"
echo "# executing: $COMMAND \&\> ${LOGDIR}/${RUN}.VALIDATE.log"
$COMMAND &> ${LOGDIR}/${RUN}.VALIDATE.log

# exit if file is OK
echo -e "# TEST FILE\n# grep 'is consistent' ${LOGDIR}/${RUN}.VALIDATE.log"
if [[ $(grep 'is consistent' ${LOGDIR}/${RUN}.VALIDATE.log) ]]; then
	echo -e "\n###STATUS###\n${RUN}\tOK\n#########\n"
	exit 0
fi

echo -e "\n###STATUS###\n${RUN}\tFAILURE\tSRA FILE IS NOT CONSISTENT\n#########\n"

exit 1
